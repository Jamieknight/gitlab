export const DRAGGABLE_TAG = 'div';

/* eslint-disable @gitlab/require-i18n-strings */
export const EpicFilterType = {
  any: 'Any',
  none: 'None',
};

export const IterationFilterType = {
  any: 'Any',
  none: 'None',
  current: 'Current',
};

export const GroupByParamType = {
  epic: 'epic',
};

export default {
  DRAGGABLE_TAG,
  EpicFilterType,
};
